"""
    Created by mbenlioglu on 2/27/2018
"""
import requests

from celery import Celery
from celery.schedules import crontab

# Initialize Celery
app = Celery(__name__, broker='redis://localhost:6379/0', backend='redis://localhost:6379/0')

API_URL = 'http://127.0.0.1:5000'


@app.task
def add_flights():
    endpoint = '/flights/add'
    response = requests.get(API_URL+endpoint, params={'count': 10})
    if response.ok:
        result = response.json()
        print result
    else:
        print response.status_code + ': ' + response.json()


# Calls test('hello') every 10 seconds.
app.add_periodic_task(crontab(), add_flights.s(), name='10-flights-every-min')
