"""
author: mertkosan
createdAt: 2/27/2018
"""
import requests

application_description = """
Welcome to MM Airlines Online Ticket Application
"""

application_options = """
Available options (Please use commands in parenthesis):
1) Look Flights (F)
2) Look Flight (F <flight_id>)
3) Buy Tickets (BT <flight_id> <seat_number(optional)>)
4) Look Ticket (T <ticket_id>)
5) Cancel Ticket (CT <ticket_id>)
6) Exit Application (exit)
"""

good_bye = """
Good Bye!
"""

load_message = 'Loading...\n'

API_URL = 'http://127.0.0.1:5000'


def wrong_command():
    print "You enter wrong option!"


def get_response(url):
    response = requests.get(url)
    if response.ok:
        result = response.json()
        print result
    else:
        print response.status_code, ':', response.json()


def delete_response(url):
    response = requests.delete(url)
    if response.ok:
        result = response.json()
        print result
    else:
        print wrong_command()


def get_flight(flight_id=None):
    get_flight_url = API_URL + "/flights" if flight_id is None else API_URL + "/flight/" + str(flight_id)
    get_response(get_flight_url)


def buy_ticket(flight_id, seat_number=None):
    buy_ticket_url = API_URL + "/flight/" + str(flight_id) + "/buy"
    buy_ticket_url = buy_ticket_url if seat_number is None else buy_ticket_url + "/" + str(seat_number)
    get_response(buy_ticket_url)


def handle_ticket(ticket_id, cancel=False):
    handle_ticket_url = API_URL + "/ticket/" + str(ticket_id)
    get_response(handle_ticket_url) if not cancel else delete_response(handle_ticket_url)


if __name__ == '__main__':
    print application_description
    print application_options
    exit_app = False
    while not exit_app:
        commands = raw_input('> ').split(" ")
        print load_message
        len_commands = len(commands)
        option = commands[0].upper()
        if option == "F":
            if len_commands == 1:
                get_flight()
            elif len_commands == 2:
                get_flight(commands[1])
            else:
                wrong_command()
        elif option == "BT":
            if len_commands == 2:
                buy_ticket(commands[1])
            elif len_commands == 3:
                buy_ticket(commands[1], commands[2])
            else:
                wrong_command()
        elif option == "T":
            if len_commands == 2:
                handle_ticket(commands[1])
            else:
                wrong_command()
        elif option == "CT":
            if len_commands == 2:
                handle_ticket(commands[1], cancel=True)
            else:
                wrong_command()
        elif option == "EXIT" and len_commands == 1:
            exit_app = True
        else:
            wrong_command()

    print good_bye
