"""
    Created by mbenlioglu on 2/26/2018
"""
import time
import random
import uuid

from datetime import datetime, timedelta

from flask import Flask, request, jsonify


# databases :)
flights = {}  # uid: {dest, from, date, nans}
flight_schedule = {}  # date: [flight_ids]
tickets = {}  # uid: {fuid, price, seat_no}


app = Flask(__name__)


def get_date(millis):
    elapsed = millis - epoch
    return datetime.fromtimestamp(elapsed * 24 * 60 + epoch)


def get_formatted_date(millis):
    return datetime.fromtimestamp(millis).strftime('%d.%m.%y - %H:%M')


def now():
    return int(round(time.time()))


def date_to_seconds(date):
    utcepoch = datetime.fromtimestamp(0)
    return int(round((date - utcepoch).total_seconds()))


def get_seat_price(flight_id):
    return (50 - len(flights[flight_id]['available_seats']) + 1) * 10


@app.route('/flights', methods=['GET'])
def get_flights():
    return jsonify([{'dest': flights[key]['dest'],
                     'from': flights[key]['from'],
                     'date': get_formatted_date(flights[key]['date']),
                     'uid': key} for key in flights])


@app.route('/flights/add', methods=['GET'])
def add_flight():
    count = int(request.args.get('count')) or 1
    # new flight is added to a date starting from 2 days from now, until the day after it
    start_date = get_date(now()) + timedelta(2)
    start_date = datetime(start_date.year, start_date.month, start_date.day)
    if flight_schedule.get(start_date) is None:
        flight_schedule[start_date] = []
    elif len(flight_schedule[start_date]) >= 10:
        return jsonify({'msg': 'Daily quota is exceed for flights! Wait next day to add new flights'})

    for i in xrange(min(10 - len(flight_schedule[start_date]), count)):
        uid = str(uuid.uuid4())
        flights[uid] = {}
        flights[uid]['dest'] = ''.join(chr(random.randrange(65, 91)) for _ in range(3))
        flights[uid]['from'] = ''.join(chr(random.randrange(65, 91)) for _ in range(3))
        flights[uid]['available_seats'] = set(range(1, 51))

        # Choose a random date starting from 2 days from now, until the day after it
        flights[uid]['date'] = random.randrange(date_to_seconds(start_date), date_to_seconds(start_date + timedelta(1)))
        flight_schedule[start_date].append(uid)
    return jsonify({'msg': 'OK'})


@app.route('/flight/<string:flight_id>', methods=['GET'])
def get_flight(flight_id):
    f = flights.get(flight_id)
    if f is None:
        return jsonify({'msg': 'No flight with given id!'})
    else:
        f['uid'] = flight_id
        f['price'] = get_seat_price(flight_id)
        response = {key: f[key] for key in f}
        response['available_seats'] = list(response['available_seats'])
        return jsonify(response)


@app.route('/flight/<string:flight_id>/buy', methods=['GET'])
@app.route('/flight/<string:flight_id>/buy/<int:seat_number>', methods=['GET'])
def buy_ticket(flight_id, seat_number=None):
    f = flights.get(flight_id)

    # Check flight_id correction
    if f is None:
        return jsonify({'msg': 'No flight with given id!'})

    # Check if we are 1 day before flight
    flight_day = datetime.fromtimestamp(f['date'])
    flight_day = datetime(flight_day.year, flight_day.month, flight_day.day)
    if not flight_day > get_date(now()) >= flight_day - timedelta(1):
        return jsonify({'msg': 'You can only buy ticket one day before flight!'})

    # Check if there is any available seats
    if len(f['available_seats']) < 1:
        return jsonify({'msg': 'No available seats for this flight!'})

    # Get random seat no / Check given seat no
    if seat_number is None:
        seat_number = random.choice(list(f['available_seats']))
    elif not 0 < seat_number <= 50:
        return jsonify({'msg': 'Invalid seat number!'})
    elif seat_number not in f['available_seats']:
        return jsonify({'msg': 'This seat is not available to buy!'})

    ticket_id = str(uuid.uuid4())
    tickets[ticket_id] = {
        'flight_uid': flight_id,
        'seat_number': seat_number,
        'price': get_seat_price(flight_id)
    }

    f['available_seats'].remove(seat_number)
    return jsonify({
        'msg': 'OK',
        'flight_uid': flight_id,
        'ticket_uid': ticket_id,
        'seat_number': seat_number
    })


@app.route('/ticket/<string:ticket_id>', methods=['GET', 'DELETE'])
def handle_ticket(ticket_id):
    ticket = tickets.get(ticket_id)
    if ticket is None:
        return jsonify({'msg': 'No ticket with given id!'})

    if request.method == 'GET':
        return jsonify({
            'dest': flights[ticket['flight_uid']]['dest'],
            'from': flights[ticket['flight_uid']]['from'],
            'price': ticket['price'],
            'seat_number': ticket['seat_number'],
            'flight_uid': ticket['flight_uid'],
            'date': get_formatted_date(flights[ticket['flight_uid']]['date'])
        })
    elif request.method == 'DELETE':
        flights[ticket['flight_uid']]['available_seats'].add(ticket['seat_number'])
        del tickets[ticket_id]
        return jsonify({'msg': 'OK'})


@app.route('/admin/details', methods=['GET'])
def get_all_sales():
    response = [{
        'flight_uid': fkey,
        'date': get_formatted_date(flights[fkey]['date']),
        'from': flights[fkey]['from'],
        'dest': flights[fkey]['dest'],
        'tickets': [{
            'ticket_uid': tkey,
            'seat_number': tickets[tkey]['seat_number'],
            'price': tickets[tkey]['price']
        } for tkey in tickets if tickets[tkey]['flight_uid'] == fkey]
    } for fkey in flights]
    return jsonify(response)


if __name__ == '__main__':
    epoch = now()
    epoch_date = datetime.fromtimestamp(epoch)

    app.run(debug=True, threaded=True)
