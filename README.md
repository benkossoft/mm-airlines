# mm-airlines

&nbsp;&nbsp;&nbsp;&nbsp;
MM-airlines is a simple, Flask based RESTful server implementation that provides some endpoints that can be used in
commercial flight applications.

**Implemented by:**

 * [M.Mucahid Benlioglu](https://github.com/mbenlioglu)
 * [Mert Kosan](https://github.com/mertkosan)


## Getting Started

**Prerequisites:**

- [Python](https://docs.python.org/2/) (developed and tested in python 2.7)
- [pip](https://pip.pypa.io/en/stable/) for python package management

### Installation and Running

&nbsp;&nbsp;&nbsp;&nbsp;
Firstly, clone or download this repository, then run the following command in the project root to install needed
dependencies
    
    $ pip install -r requirements.txt

After installation succeeds, you can start the server by executing the following command:
    
    $ python server.py

When server comes online, a celery scheduler can be activated, which adds 10 flights daily (every minute for simulated
environment) with the following command (Linux only):
 
    $ celery -A kereviz worker -B

You can test the endpoints by provided client with the following command and following the
instructions or can use external application

    $ python client.py 
